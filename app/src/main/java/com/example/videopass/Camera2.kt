package com.example.videopass

import android.annotation.SuppressLint
import android.content.Context
import android.hardware.camera2.CameraCaptureSession
import android.hardware.camera2.CameraDevice
import android.hardware.camera2.CameraManager
import android.hardware.camera2.params.OutputConfiguration
import android.hardware.camera2.params.SessionConfiguration
import android.media.MediaCodec
import android.media.MediaCodecInfo
import android.media.MediaFormat
import android.net.wifi.WifiManager
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.example.videopass.databinding.FragmentCamera2Binding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import java.net.InetAddress
import java.net.ServerSocket
import java.net.Socket
import java.net.SocketException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.concurrent.Executors
import kotlin.concurrent.thread
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


class Camera2 : Fragment() {
	private val args: Camera2Args by navArgs()

	private var _binding: FragmentCamera2Binding? = null
	private val binding get() = _binding!!

	private val cameraManager: CameraManager by lazy {
		val context = requireContext().applicationContext
		context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
	}
	private val cameraThread = HandlerThread("CameraThread").apply { start() }
	private val cameraHandler = Handler(cameraThread.looper)

	private var broadcast: Broadcast? = null

	private var session: CameraCaptureSession? = null
	private var camera: CameraDevice? = null
	private var codec: MediaCodec? = null

	private lateinit var socket: ServerSocket
	private var client: Socket? = null

	private lateinit var previewSurface: AutoFitSurfaceView
	private var encoderSurface: Surface? = null

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		val policy = ThreadPolicy.Builder().permitAll().build()
		StrictMode.setThreadPolicy(policy)

		previewSurface = binding.autoFitSurfaceView
		previewSurface.holder?.addCallback(object : SurfaceHolder.Callback {
			override fun surfaceDestroyed(holder: SurfaceHolder) = Unit
			override fun surfaceChanged(
				holder: SurfaceHolder,
				format: Int,
				width: Int,
				height: Int
			) = Unit

			override fun surfaceCreated(holder: SurfaceHolder) {
				val previewSize = getPreviewOutputSize(
					previewSurface.display,
					cameraManager.getCameraCharacteristics(args.id),
					SurfaceHolder::class.java
				)
				previewSurface.setAspectRatio(previewSize.width, previewSize.height)

//				previewSurface.post {
//					initialiseEncoder()
//
//					lifecycleScope.launch(Dispatchers.IO) {
//						initialiseCamera()
//						initialiseCameraPreview()
//					}
//				}
			}
		})

		showDeviceIp()

		socket = ServerSocket(8914)
		thread(start = true) {
			listenForConnections()
		}

		broadcast = Broadcast(requireContext()).apply {
			start()
		}
	}

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View {
		_binding = FragmentCamera2Binding.inflate(inflater, container, false)
		return binding.root
	}

	override fun onDestroy() {
		super.onDestroy()

		broadcast?.close()
		codec?.stop()
		encoderSurface?.release()
		camera?.close()
		socket.close()
		cameraThread.quitSafely()
	}

	private fun cleanup() {
		codec?.stop()
		encoderSurface?.release()
		camera?.close()
	}

	private fun showDeviceIp() {
		val context = requireContext().applicationContext
		val wm = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
		val ip = InetAddress.getByAddress(
			ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN)
				.putInt(wm.connectionInfo.ipAddress).array()
		)
			.hostAddress
		binding.textView.text = ip
	}

	private fun listenForConnections() = lifecycleScope.launch(Dispatchers.IO) {
		suspendCancellableCoroutine<Unit> {
			while (it.isActive) {
				Log.d(LOG_TAG, "waiting for new client")

				var _client: Socket
				try {
					_client = socket.accept()
				} catch (e: SocketException) {
					Log.e(LOG_TAG, e.toString())
					break
				}
				val data = ByteArray(16)

				requireActivity().runOnUiThread {
					Toast.makeText(
						context,
						"new client, address => ${_client.inetAddress}",
						Toast.LENGTH_LONG
					).show()
				}

				_client.getInputStream().read(data)

				Log.d(LOG_TAG, "new client, address => ${_client.inetAddress}")
				val width = ByteBuffer.wrap(data.sliceArray(0..3)).int
				val height = ByteBuffer.wrap(data.sliceArray(4..7)).int
				val version = ByteBuffer.wrap(data.sliceArray(8..11)).int
				val nonce = ByteBuffer.wrap(data.sliceArray(12..15)).int

				Log.d(
					LOG_TAG,
					"resolution => ${width}x${height}, version => $version, nonce => $nonce"
				)

				client = _client
				initialiseEncoder()

				this.launch {
					initialiseCamera()
					initialiseCameraPreview()
				}
			}
		}
	}

	private fun initialiseEncoder() {
		Log.d(LOG_TAG, "creating codec")
		codec = MediaCodec.createEncoderByType("video/hevc")

		Log.d(LOG_TAG, "creating format for the codec")
		val format = MediaFormat.createVideoFormat("video/hevc", args.width, args.height).apply {
			setInteger(
				MediaFormat.KEY_COLOR_FORMAT,
				MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Flexible
			)
			setInteger(MediaFormat.KEY_FRAME_RATE, 30)
			setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 6)
		}

		Log.d(LOG_TAG, "configuring codec")
		codec!!.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE)
		encoderSurface = codec!!.createInputSurface()
		codec!!.setCallback(object : MediaCodec.Callback() {
			val eightNoughts = ByteArray(8)
			val lenBuffer = ByteBuffer.allocate(4).apply {
				order(ByteOrder.BIG_ENDIAN)
			}

			override fun onInputBufferAvailable(codec: MediaCodec, index: Int) {}

			override fun onOutputBufferAvailable(
				codec: MediaCodec,
				index: Int,
				info: MediaCodec.BufferInfo
			) {
				// buffer is always a complete frame (one or more NAL units)
				val buffer = codec.getOutputBuffer(index)
				val bytes = ByteArray(info.size)
				buffer!!.get(bytes)

				codec.releaseOutputBuffer(index, false)
				// Log.d(LOG_TAG, "got buffer of size ${info.size}")

				if (client != null) {
					val stream = client!!.getOutputStream()
					try {
						stream.write(eightNoughts)
						lenBuffer.putInt(info.size)
						stream.write(lenBuffer.array())
						lenBuffer.clear()
						stream.write(bytes)
					} catch (e: SocketException) {
						client!!.close()
						cleanup()
						client = null
					}
				}
			}

			override fun onError(codec: MediaCodec, error: MediaCodec.CodecException) {
				Log.e(LOG_TAG, "Error: $error")
			}

			override fun onOutputFormatChanged(codec: MediaCodec, format: MediaFormat) {
				Log.d(LOG_TAG, "encoder format changed: $format")
			}
		})

		codec!!.start()
	}

	@SuppressLint("MissingPermission")
	private suspend fun initialiseCamera() {
		camera = suspendCancellableCoroutine {
			Log.d(LOG_TAG, "opening camera")
			cameraManager.openCamera(args.id, object : CameraDevice.StateCallback() {
				override fun onOpened(device: CameraDevice) = it.resume(device)

				override fun onDisconnected(device: CameraDevice) {
					Log.d(LOG_TAG, "camera disconnected")
					requireActivity().finish()
				}

				override fun onError(device: CameraDevice, error: Int) {
					Log.d(LOG_TAG, "camera errored")
					val msg = when (error) {
						ERROR_CAMERA_DEVICE -> "Fatal (device)"
						ERROR_CAMERA_DISABLED -> "Device policy"
						ERROR_CAMERA_IN_USE -> "Camera in use"
						ERROR_CAMERA_SERVICE -> "Fatal (service)"
						ERROR_MAX_CAMERAS_IN_USE -> "Maximum cameras in use"
						else -> "Unknown"
					}
					val exc = RuntimeException("Camera $args.id error: ($error) $msg")
					if (it.isActive) it.resumeWithException(exc)
				}
			}, cameraHandler)
		}
	}

	private suspend fun initialiseCameraPreview() {
		session = suspendCoroutine {
			camera!!.createCaptureSession(SessionConfiguration(
				SessionConfiguration.SESSION_REGULAR,
				listOf(
					OutputConfiguration(previewSurface.holder.surface),
					OutputConfiguration(encoderSurface!!)
				),
				Executors.newSingleThreadExecutor(),
				object : CameraCaptureSession.StateCallback() {
					override fun onConfigured(session: CameraCaptureSession) = it.resume(session)

					override fun onConfigureFailed(session: CameraCaptureSession) {
						val exc =
							RuntimeException("Camera ${camera!!.id} session configuration failed")
						it.resumeWithException(exc)
					}
				}
			))
		}

		val previewRequest = camera!!.createCaptureRequest(CameraDevice.TEMPLATE_RECORD).apply {
			addTarget(previewSurface.holder.surface)
			addTarget(encoderSurface!!)
		}.build()
		session!!.setRepeatingRequest(previewRequest, null, cameraHandler)
	}

	companion object {
		const val LOG_TAG = "Camera2Fragment"
	}
}
