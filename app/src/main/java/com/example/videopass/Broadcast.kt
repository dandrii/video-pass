package com.example.videopass

import android.content.Context
import android.net.wifi.WifiManager
import android.util.Log
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress


class Broadcast(private val context: Context) : Thread() {
	private lateinit var socket: DatagramSocket;

	private fun getBroadcastAddress(): InetAddress? {
		val wifi = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
		val dhcp = wifi.dhcpInfo
		val broadcast = dhcp.ipAddress and dhcp.netmask or dhcp.netmask.inv()
		val quads = ByteArray(4)
		for (k in 0..3) quads[k] = (broadcast shr k * 8 and 0xFF).toByte()
		return InetAddress.getByAddress(quads)
	}

	override fun run() {
		socket = DatagramSocket()
		socket.broadcast = true
		val packet =
			DatagramPacket("video_srv:greet\u0000".toByteArray(), 16, getBroadcastAddress(), 8915)

		Log.d(LOG_TAG, "starting broadcast")

		while (!socket.isClosed) {
			Log.d(LOG_TAG, "sending broadcast message")
			socket.send(packet)
			sleep(5000)
		}
	}

	fun close() {
		socket.close()
	}

	companion object {
		const val LOG_TAG = "Broadcast"
	}
}