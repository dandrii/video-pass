package com.example.videopass

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.media.MediaRecorder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation

private const val PERMISSIONS_REQUEST_CODE = 10
private val PERMISSIONS_REQUIRED = arrayOf(
	Manifest.permission.CAMERA,
	Manifest.permission.RECORD_AUDIO
)

class CameraList : Fragment() {
	private val cameraManager: CameraManager by lazy {
		requireContext().getSystemService(Context.CAMERA_SERVICE) as CameraManager
	}

	private fun lensOrientationString(value: Int) = when (value) {
		CameraCharacteristics.LENS_FACING_BACK -> "Back"
		CameraCharacteristics.LENS_FACING_FRONT -> "Front"
		CameraCharacteristics.LENS_FACING_EXTERNAL -> "External"
		else -> "Unknown"
	}

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? = inflater.inflate(R.layout.fragment_camera_list, container, false)

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		if (!hasPermissions(requireContext())) {
			requestPermissions(PERMISSIONS_REQUIRED, PERMISSIONS_REQUEST_CODE)
		}
	}

	override fun onRequestPermissionsResult(
		requestCode: Int,
		permissions: Array<String>,
		grantResults: IntArray
	) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults)
		if (requestCode == PERMISSIONS_REQUEST_CODE) {
			if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
				Toast.makeText(context, "Permission request denied", Toast.LENGTH_LONG).show()
			}
		}
	}

	private fun hasPermissions(ctx: Context) = PERMISSIONS_REQUIRED.all {
		ContextCompat.checkSelfPermission(ctx, it) == PackageManager.PERMISSION_GRANTED
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		val list = requireView().findViewById<LinearLayout>(R.id.list)
		cameraManager.cameraIdList.forEach { id ->
			val characteristics = cameraManager.getCameraCharacteristics(id)
			val orientation = lensOrientationString(
				characteristics.get(CameraCharacteristics.LENS_FACING)!!
			)

			// Query the available capabilities and output formats
			val capabilities = characteristics.get(
				CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES
			)!!
			val cameraConfig = characteristics.get(
				CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP
			)!!

			// Return cameras that declare to be backward compatible
			if (capabilities.contains(
					CameraCharacteristics
						.REQUEST_AVAILABLE_CAPABILITIES_BACKWARD_COMPATIBLE
				)
			) {
				// Recording should always be done in the most efficient format, which is the format native to the camera framework
				val targetClass = MediaRecorder::class.java

				// For each size, list the expected FPS
				cameraConfig.getOutputSizes(targetClass).forEach { size ->
					// Get the number of seconds that each frame will take to process
					val secondsPerFrame =
						cameraConfig.getOutputMinFrameDuration(targetClass, size) /
								1_000_000_000.0
					// Compute the frames per second to let user select a configuration
					val fps = if (secondsPerFrame > 0) (1.0 / secondsPerFrame).toInt() else 0
					val fpsLabel = if (fps > 0) "$fps" else "N/A"

					val button = Button(context)
					button.text = "$orientation ($id) $size $fpsLabel FPS"
					button.setOnClickListener {
						val bundle = bundleOf(
							"id" to id,
							"height" to size.height,
							"width" to size.width,
							"fps" to fps
						)
						Navigation.findNavController(requireActivity(), R.id.fragmentContainer)
							.navigate(R.id.action_cameraList_to_camera, bundle)
					}
					list.addView(button)
				}
			}
		}
	}
}